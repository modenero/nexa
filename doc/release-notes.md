Release Notes for Nexa 1.3.0.3
======================================================

Nexa version 1.3.0.3 is now available from:

  <https://gitlab.com/nexa/nexa/-/releases>

Please report bugs using the issue tracker at github:

  <https://gitlab.com/nexa/nexa/-/issues>

This is minor release of Nexa, for more information about Nexa see:

- https://nexa.org
- https://spec.nexa.org

Main changes in 1.3.0.3
-----------------------

This is list of the main changes that have been merged in this release:

- fix inconsistent script flags
- bump rostrum version to 9.0.0
- make it possible to restore wallet from Nexa-Qt
- improve usability of wallet recovery procedure
- improve rostrum documentation
- make libnexa to be built and available on all platforms
- update falcon512 implementation to use PQClean code base
- misc code cleaning and bug fixes

Commit details
--------------

- `8aa9d63999` Bump nexa to version 1.3.0.3 (Andrea Suisani)
- `694ef5c795` Pin rostrum electrum server to v9.0 (Andrea Suisani)
- `b4610a7697` make the script flags we validate blocks against const (Griffith)
- `6f5e4fe286` fix inconsistent script flags.  CDS is mandatory on since we have had it since genesis block. (Andrew Stone)
- `153d4ab7c4` IsTrusted() needs to be true when doing FilterCoins() (Peter Tschipper)
- `fa1cb93532` Add a Restore Wallet menu item to Nexa-QT (Peter Tschipper)
- `e57e5b09aa` docs for Setting up the Nexa Full Node on Linux for regtest (Jørgen Svennevik Notland)
- `2ea4d410cc` Fix serializationOp for CGroupTokenID (Peter Tschipper)
- `4ec40bfccb` Fix -Wsign-compare warn in cashlib.cpp (Andrea Suisani)
- `60359e2149` Update Electrum integration documentation (Andrea Suisani)
- `5a722e89b2` Clear all addrman data structures when calling Clear() (Peter Tschipper)
- `5326d31b27` Add OSX/macos and other "native" functions to match those functions defined using Java symbols; move to libnexakotlin (v2) underlying API. (Andrew Stone)
- `d6947380a7` Prevent the token info from getting nulled (Peter TSchipper)
- `7f9ad9481b` Remove leftover from the SigOps counting machinery (Andrea Suisani)
- `19c159faf4` Move dsproof files to /respend folder (Peter Tschipper)
- `85a5d85859` Add "doublespent" to help text for gettxpoolentry (Peter TSchipper)
- `18cf6ab183` Fix spurious failure of bip69 (Peter TSchipper)
- `90e9d050fe` Cleanup forks.cpp/.h (Peter Tschipper)
- `fbee888fb4` Clarify the message returned after a wallet is encrypted (Peter TSchipper)
- `6a03c155b4` Show cashaddr default to true in command line list (Peter TSchipper)
- `9f8a265779` Refactor script destinations (Griffith)
- `01faa0c774` remove TORV2, it is insecure and no longer supported by TOR (Griffith)
- `bb8c573b8c` Fix warnings spotted by clang 13.0.0 in netaddress.h (Andrea Suisani)
- `308f95ff5e` Print out the dumpfile location when dumping the wallet (Peter Tschipper)
- `6bf4018123` Fix bug in -salvagewallet (Peter Tschipper)
- `9de72b880f` refactor some classes into their own headers to cut down on includes for libraries (Griffith)
- `dffdfdcdff` convert CNetAddr::ip from a c-style byte array to a byte vector and other small cleanup (Griffith)
- `de5adec407` Compilation warning fixes (Griffith)
- `4592e46f1a` Bip155 implementation (Griffith)
- `b3a2cef7c0` Add cashlib function to run a data specific schnorr signature check, and squelch a spammy log (Andrew Stone)
- `f0cfbbcee3` move fundamental type data members from chainparams to basechainparams (Griffith)
- `9656b9616e` Remove unused extern MAX_FEE_PERCENT_OF_VALUE from globals.cpp (Peter TSchipper)
- `05a4d554d6` Remove LOGA from ismine.cpp (Peter Tschipper)
- `8571e88389` When importing a privkey make sure to set the address to a p2pkt type (Peter Tschipper)
- `97de2ef615` Remove boost from chainparams.cpp (Dylan Aird)
- `70192c8884` Fix bug in auto consolidation when sending the full wallet balance (Peter Tschipper)
- `e9ad4e755a` Pass P2P port to rostrum (Dagur Valberg Johannsson)
- `67d19e6bef` Fix old bug in zmq interface tests (Peter TSchipper)
- `048f1c9d79` Fix small bug in coin selection when creating transactions (Peter TSchipper)
- `3da790d600` Handle orphan transactions in CommitTransaction() (Peter TSchipper)
- `8fc3bd4a05` [ci] Add a new task: test-ubuntu-qa-extended (Andrea Suisani)
- `e1f718b056` fix how to run formatting instruction. old command depends on location (Griffith)
- `4d42e6cc52` Improve the performance of returning different balance types (Peter TSchipper)
- `ef590a0a0a` Fix native compilation on osx (Andrea Suisani)
- `15300e166f` enable zapwallettxes (Griffith)
- `61116af6ba` Add not_so_big_wallet_4node.py to the suite of extended tests (Andrea Suisani)
- `b4d53da2af` Remove unused command line option: permitbaremultisig (Peter TSchipper)
- `cb896592f8` Fix for sendtoaddress() when simultaneous consolidat() happens (Peter TSchipper)
- `f904dc8973` Fix numerous compile errors in falcon files (Peter TSchipper)
- `1a5f734c33` Rename any falcon *.c files to falcon *.cpp (Peter TSchipper)
- `9b31fe9511` Update falcon512 files with PQClean code base (Peter Tschipper)

Credits
-------

Thanks to everyone who directly contributed to this release:

- Andrea Suisani
- Andrew Stone
- Dagur Valberg Johannsson
- Dylan Aird
- Griffith
- Jørgen Svennevik Notland
- Peter Tschipper
